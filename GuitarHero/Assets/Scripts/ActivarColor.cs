﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarColor : MonoBehaviour
{
    public AudioSource audioPressionado;
    public AudioSource audioActierto;
    public KeyCode tecla;
    bool activo = false;
    GameObject nota;
    SpriteRenderer spriteBoton;
    Color colorBase;
    public bool creador;

    private void Awake()
    {
        spriteBoton = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("Puntos", 000000000);
        colorBase = spriteBoton.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (creador)
        {

        }
        else
        {
            if (Input.GetKeyDown(tecla) && activo)
            {
                Destroy(nota);
                añadirPuntos();
                audioActierto.Play();
                activo = false;
            }
            if (Input.GetKeyDown(tecla))
            {
                audioPressionado.Play();
                StartCoroutine(presionado());
                if (PlayerPrefs.GetInt("Puntos") >= 20)
                {
                    restarPuntos();
                }
            }
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        activo = true;
        if (collision.gameObject.tag == "nota")
        {
            nota = collision.gameObject;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        activo = false;
    }

    IEnumerator presionado()
    {
        spriteBoton.color = new Color(0,0,0);
        yield return new WaitForSeconds(0.05f);
        spriteBoton.color = colorBase;
    }
    
    void añadirPuntos()
    {
        PlayerPrefs.SetInt("Puntos", PlayerPrefs.GetInt("Puntos") + 100);
    }
    void restarPuntos()
    {
        PlayerPrefs.SetInt("Puntos", PlayerPrefs.GetInt("Puntos") - 20);
    }
}
