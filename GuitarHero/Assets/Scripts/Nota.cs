﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nota : MonoBehaviour
{
    Rigidbody2D rigidbody;
    public float velocidad;
    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
        rigidbody.velocity = new Vector2(0, -velocidad);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
